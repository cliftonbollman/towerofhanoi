//declare a counter 
let tower1 = document.getElementById("tower1");
let disc1 = document.getElementById("disc1");
let disc2 = document.getElementById("disc2");
let disc3 = document.getElementById("disc3");
let disc4 = document.getElementById("disc4");
let tower2 = document.getElementById("tower2");
let tower3 = document.getElementById("tower3");
let counter = "pickUp";
let holder = null;
let count = 0;
//displays the number of moves needed to get from tower1 to tower3
function display(){
    let result = document.getElementById("result");
    result.textContent = "Number of moves : " + count;
    setTimeout(reset,2000);
}
//function resets the divs to the initial condition
function reset(){
    tower1.appendChild(disc1);
    tower1.appendChild(disc2);
    tower1.appendChild(disc3);
    tower1.appendChild(disc4);
    result.textContent = "";
    counter = "pickUp";

}
//function to pick up the div from the start tower
function pickUpDiv(startDiv) {
    let noOfDivs = startDiv.childElementCount;
    if (noOfDivs === 0) {
        alert("This tower is empty, Click another tower");
        
    }
    else {
        holder = startDiv.lastElementChild;
        counter = "putDown";
        
    }
    count++;
    if(tower1.childElementCount===0 && tower3.childElementCount===4){
        display();
    }
}
//function to put down the div on the destination tower
function putDownDiv(endDiv) {
    let lastElement = endDiv.lastElementChild;
    if(lastElement === null ){
        endDiv.appendChild(holder);
        counter = "pickUp";
        
    }
   else {
       
       let curWidth = lastElement.clientWidth;
       let holderWidth = holder.clientWidth;
       if(curWidth <= holderWidth){
        alert("Cannot put on top of the current div, choose another tower");
        counter = "pickUp";
        
       }
       else{
        endDiv.appendChild(holder);
        counter = "pickUp";
        
       }
    }
    count++;
    if(tower1.childElementCount===0 && tower3.childElementCount===4){
        display();
    }
}

//Choose to pick up div or put the div down when the tower is clicked
function shuffle(event) {
    console.log(counter);
    if(counter === "pickUp"){
         pickUpDiv(event.currentTarget);
    }
    else if(counter === "putDown"){
        putDownDiv(event.currentTarget);
    }
}
 //adding event listeners to the towers 
let towers = document.querySelectorAll(".tower");

for (var i = 0; i < towers.length; i++) {
    
    towers[i].addEventListener('click', shuffle);
}

//adding event listener to the reset button
document.querySelector("#button1").addEventListener("click", reset);
//adding event listener to the undo button
document.querySelector("#button2").addEventListener("click",function(){

if(counter === "pickUp"){
   counter = "putDown";
}
if(count === 0 || counter === "putDown"){
    counter = "pickUp";
}
})







